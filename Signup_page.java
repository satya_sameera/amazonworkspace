package Pages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import basepackage.Base;

public class Signup_page extends Base {
	
	
	
	WebDriver driver;
	public Signup_page(WebDriver driver) {
		
		this.driver = driver;
		
		
	}
			//Details of the elements that we are accessing 
			
			By signin= By.xpath("/html/body/div[1]/header/div/div[1]/div[3]/div/a[2]");
			
			
			By signup= By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div/span/span/a");
			
			 
			By name=By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div/form/div/div/div[1]/input");
			By contact= By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div/form/div/div/div[2]/div/div/div/div[2]/input");
			By Email= By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div/form/div/div/div[3]/div/input");
			By password= By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div/form/div/div/div[4]/div/input");
			By submit= By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div/form/div/div/div[6]/span/span/input");
			
			By search=By.xpath("/html/body/div[1]/header/div/div[1]/div[2]/div/form/div[2]/div[1]/input");
			By item=By.xpath("/html/body/div[1]/header/div/div[1]/div[2]/div/form/div[3]/div/span/input");
			
			By bestseller_items = By.xpath("//span[text()='Best seller'] /following::div[2]");
			

			By resignin=By.xpath("//*[@id=\"authportal-main-section\"]/div[2]/div/div[2]/div[2]/div[1]/a");
			By signin_name=By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div[1]/form/div/div/div/div[1]/input[1]");
			By proceed=By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div/div[1]/form/div/div/div/div[2]/span/span/input");
			By enter_password=By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div[1]/div/div/form/div/div[1]/input");
			By proceed_to=By.xpath("/html/body/div[1]/div[1]/div[2]/div/div[2]/div[1]/div/div/form/div/div[2]/span/span/input");
			
			By cart=By.xpath("/html/body/div[2]/header/div/div[1]/div[3]/div/a[5]");
			
			
			
			public WebElement getsignin()
			{
				//driver.get("https://www.amazon.in/");

				return driver.findElement(signin);
			}
			public WebElement getsignup()
			{
				return driver.findElement(signup);
			}
			public WebElement getname()
			{
				return driver.findElement(name);
			}
			public WebElement getcontact()
			{
				return driver.findElement(contact);
			}
			public WebElement getemail()
			{
				return driver.findElement(Email);
			}
			public WebElement getpassword()
			{
				return driver.findElement(password);
			}
			public WebElement getsubmit()
			{
				return driver.findElement(submit);
			}
			public WebElement proceed()
			{
				return driver.findElement(proceed);
			}
			public WebElement resignin()
			{
				return driver.findElement(resignin);
			}
			public WebElement signin_name()
			{
				return driver.findElement(signin_name);
			}
			public WebElement enter_password()
			{
				return driver.findElement(enter_password);
			}
			public WebElement proceed_to()
			{
				return driver.findElement(proceed_to);
			}
			public WebElement getsearch()
			{
				return driver.findElement(search);
			}
			public WebElement getitem()
			{
				return driver.findElement(item);
			}
			
			public List<WebElement> Bestseller_items()
			{
				return driver.findElements(bestseller_items);
			}
			
			public int count()
			{
				return driver.findElements(bestseller_items).size();
			}
			public WebElement cart()
			{
				return driver.findElement(cart);
			}
			
			
		}



