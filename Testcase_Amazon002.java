package Test;

import java.util.Iterator;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import Pages.Signup_page;
import basepackage.Base;
import io.github.bonigarcia.wdm.WebDriverManager;

public class Testcase_Amazon002 extends Base{
	
	
	
	@Test
	public void register_bestseller()
	{
		

		
		//create object for Signup_page(where elements are defined)
		
		Signup_page t1=new Signup_page(driver);
		credentials c=new credentials();
		
		t1.getsignin().click();
		t1.getsignup().click();
		
		//Enter credentials to the website
		
		t1.getname().sendKeys(c.username);
		t1.getcontact().sendKeys(c.contact);
		t1.getpassword().sendKeys(c.password);
		
		//submit the credentials
		t1.getsubmit().click();
		
		//As we cannot automatically enter otp for a new register use,I'm going with already registered user credentials
		t1.resignin().click();
		t1.signin_name().sendKeys(c.contact);
		t1.proceed().click();
		t1.enter_password().sendKeys(c.pswrd);
		t1.proceed_to().click();
		
		//search for headphones
		
		t1.getsearch().sendKeys("headphones");
		t1.getitem().click();
		
		//select items with mark "Best seller"  [Can be writen in common_utils class but readability i have writen here]
		
		for(int i=0;i<t1.count();i++)
		{
		t1.Bestseller_items().get(i).click();
		
		System.out.println(i);
					}
		window_handle();
		
	}
	
	//Handle multiple-windows 
	//[Can be writen in common_utils class but readability i have writen here]
	public static void window_handle() {
		
		
		Set<String> s=driver.getWindowHandles();
		Iterator<String> ids=s.iterator();
		ids.next();
		
		//Add items into cart 
		for(int i=0;ids.hasNext();i++) {
			
			driver.switchTo().window(ids.next());
			
			
			
			driver.findElement(By.xpath("//*[@id='add-to-cart-button' or @title='Add to Shopping Cart']")).click();
					
		}
		}
	
		
		
		
	}

